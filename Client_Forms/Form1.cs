﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using openalprnet;
using Emgu.CV;
using System.Net.Http;

namespace Client_Forms
{
    public partial class Form1 : Form
    {
        Capture webcam;
        Mat lastFrame = new Mat();
        AlprNet alpr;
        readonly HttpClient client = new HttpClient();
        string sendPath;
        Dictionary<string, float> lastResults = new Dictionary<string, float>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            webcam = new Emgu.CV.Capture();
            webcam.ImageGrabbed += Webcam_ImageGrabbed;
            webcam.Start();

            alpr = new AlprNet("eu", "openalpr.conf", "/runtime_data");
            if (!alpr.IsLoaded())
            {
                throw new InvalidOperationException("OpenAlpr failed to load");
            }
            alpr.DefaultRegion = "ge";
        }

        delegate void imageGrabbedDelegate(object sender, EventArgs e);
        private void Webcam_ImageGrabbed(object sender, EventArgs e)
        {
            webcam.Retrieve(lastFrame);
            if (pictureBox1.InvokeRequired)
            {
                imageGrabbedDelegate imageGrabbedDelegate = this.Webcam_ImageGrabbed;
                pictureBox1.Invoke(imageGrabbedDelegate, sender, e);
            }
            else
            {
                pictureBox1.Image = lastFrame.Bitmap;
            }
        }

        public Image cachedImage;

        private void Path_Button_Click(object sender, EventArgs e)
        {
            Stream stream = null;

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Image files |*.jpeg;*.jpg;*.png;";
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.InitialDirectory = @"C:\Users\lucho\OneDrive\Projects (Drive)\Master_Tsu\Plates";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((stream = openFileDialog1.OpenFile()) != null)
                    {
                        textBox1.AppendText("Selected: " + openFileDialog1.FileName);
                        textBox1.AppendText(Environment.NewLine);

                        using (stream)
                        {
                            cachedImage = Image.FromStream(stream‌​);
                            pictureBox1.Image = cachedImage;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        public void RecognizeImage(string path)
        {
            if (!File.Exists(path))
            {
                textBox1.AppendText("Path does not exist");
                return;
            }

            alpr.TopN = (int)numericUpDown1.Value;

            var results = alpr.Recognize(path);
            int i = 0;
            foreach (var result in results.Plates)
            {
                textBox1.AppendText(String.Format("Plate {0}: {1} result(s)\n", i++, result.TopNPlates.Count));
                textBox1.AppendText(String.Format("  Processing Time: {0} msec(s)\n", result.ProcessingTimeMs));
                foreach (var plate in result.TopNPlates)
                {
                    textBox1.AppendText(String.Format("  - {0}\t Confidence: {1}\tMatches Template: {2}\n", plate.Characters,
                                      plate.OverallConfidence, plate.MatchesTemplate));
                }
            }
        }

        public string RecognizeImage(Bitmap image)
        {
            StringBuilder result = new StringBuilder();
            alpr.TopN = (int)numericUpDown1.Value;
            if (!alpr.IsLoaded())
            {
                result.AppendLine("OpenAlpr failed to load!");
            }
            else
            {
                var results = alpr.Recognize(image);

                if (results.Plates.Count != 0)
                {
                    int i = 0;
                    foreach (var item in results.Plates)
                    {
                        result.AppendFormat("Plate {0}: {1} result(s)" + Environment.NewLine, i++, item.TopNPlates.Count);
                        result.AppendFormat("  Processing Time: {0} msec(s)" + Environment.NewLine, item.ProcessingTimeMs);
                        foreach (var plate in item.TopNPlates)
                        {

                            result.AppendFormat("- {0}\t Confidence: {1}\tMatches Template: {2}" + Environment.NewLine,
                                                plate.Characters,
                                                plate.OverallConfidence,
                                                plate.MatchesTemplate);
                        }
                    }
                }
            }
            return result.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RecognizeImage(openFileDialog1.FileName);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync(pictureBox1.Image.Clone());
            button2.Enabled = false;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = RecognizeImage((Bitmap)e.Argument);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            button2.Enabled = true;
            textBox1.AppendText(((string)e.Result) + Environment.NewLine);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!backgroundWorker1.IsBusy)
                backgroundWorker1.RunWorkerAsync(pictureBox1.Image.Clone());
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                timer1.Interval = (int)intervalUpDown.Value;
                timer1.Start();
                button2.Enabled = false;
                intervalUpDown.Enabled = false;
            }
            else
            {
                timer1.Stop();
                button2.Enabled = true;
                intervalUpDown.Enabled = true;
            }
        }

        private void detectImagesButt_Click(object sender, EventArgs e)
        {
            autoDetectionPanel.Enabled = false;
            testingPanel.Enabled = false;
            textBox1.AppendText("Starting to detect images");
            imageScanWorker.RunWorkerAsync();
        }

        private void browseFolderButt_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                folderBox.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void imageScanWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void imageScanWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            autoDetectionPanel.Enabled = true;
            testingPanel.Enabled = true;
            textBox1.AppendText("Done" + Environment.NewLine);
        }

        private void imageScanWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var lines = File.ReadAllLines(Path.Combine(folderBox.Text, "real.txt"));
            var real = new Dictionary<string, string>(lines.Length);

            foreach (var item in lines)
            {
                var pair = item.Split(' ', '\t');
                real.Add(pair[0], pair[1]);
            }

            try
            {
                alpr.TopN = 10;
                var path = folderBox.Text;
                var files = new List<string>(Directory.GetFiles(path, "*.*", SearchOption.TopDirectoryOnly)
                      //.OrderByDescending<FileInfo>(x=>x.Name),
                      .Where(s => s.EndsWith(".png", StringComparison.OrdinalIgnoreCase) ||
                      s.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase)));

                StringBuilder results = new StringBuilder("file  result  confidence" + Environment.NewLine);

                List<AlprPlateResultNet> plates = new List<AlprPlateResultNet>();
                int i = 0;
                foreach (var file in files)
                {
                    plates.Clear();
                    plates = alpr.Recognize(file).Plates;
                    var detectionResult = "NaN";
                    float confidence = 0;
                    if (plates.Count != 0)
                    {
                        AlprPlateNet plateResult = plates[0].TopNPlates[0];
                        detectionResult = plateResult.Characters;
                        confidence = plateResult.OverallConfidence;

                        if (!plates[0].TopNPlates[0].MatchesTemplate)
                        {
                            for (int j = 1; j < plates[0].TopNPlates.Count; j++)
                            {
                                if (plates[0].TopNPlates[j].MatchesTemplate)
                                {
                                    plateResult = plates[0].TopNPlates[j];
                                    detectionResult = plateResult.Characters;
                                    confidence = plateResult.OverallConfidence;
                                    break;
                                }
                            }
                        }
                    }

                    var fileName = Path.GetFileNameWithoutExtension(file);
                    results.AppendFormat("{0}\t{1}\t{2}\t{3}{4}",
                        fileName,
                        detectionResult,
                        confidence,
                        real[fileName] == detectionResult,
                        Environment.NewLine);
                    (sender as BackgroundWorker).ReportProgress((int)(((float)i / files.Count) * 100f), null);
                    i++;
                }

                File.WriteAllText(Path.Combine(path, "results.txt"), results.ToString());
            }
            catch (Exception ex)
            {
                // throw ex;
                MessageBox.Show(ex.ToString());
                //  MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
            }
        }
    }
}
