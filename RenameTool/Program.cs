﻿using System;
using System.IO;
using System.Linq;
using System.Drawing;

namespace RenameTool
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;
            var filename = @"converted\plate{0}.jpg";

            Console.WriteLine(path);
            Console.WriteLine("Template is: '{0}'", filename);
            Console.WriteLine("Enter starting index", filename);

            if (!int.TryParse(Console.ReadLine().Trim(), out int startIndex))
            {
                Console.WriteLine("Not integer. Bye");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("Press any key to start");
            Console.ReadKey();

            var files = Directory.GetFiles(path, "*.*", SearchOption.TopDirectoryOnly)
                  //.OrderByDescending<FileInfo>(x=>x.Name),
                  .Where(s => s.EndsWith(".png", StringComparison.OrdinalIgnoreCase) ||
                  s.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase));

            foreach (var item in files)
            {
                Console.WriteLine("Found: {0}", item);
            }

            Directory.CreateDirectory(Path.Combine(path, "converted"));
            var i = 0;
            foreach (var item in files)
            {
                var image = Image.FromFile(item);
                var tempName = Path.Combine(path, String.Format(filename, (i + startIndex).ToString("00000")));
                Console.WriteLine(tempName);

                using (FileStream fs = new FileStream(tempName, FileMode.Create, FileAccess.ReadWrite))
                {
                    image.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                i++;
            }
            Console.WriteLine("Converted and renamed {0} images", i);
            Console.ReadKey();
        }
    }
}