using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Master_Backend.Models;
using System.IO;

namespace Master_Backend.Controllers
{
    [Produces("application/json")]
    [Route("api/PlateDetection")]
    public class PlateDetectionController : Controller
    {
        PlateDetectionContext _context;

        public PlateDetectionController(PlateDetectionContext context)
        {
            _context = context;

            if (_context.Items.Count() == 0)
            {
                //var results = = new Dictionary<string, float>();
                //results[""]
                //_context.Items.Add(new PlateDetection { Time = DateTime.Now, Results  });
                _context.SaveChanges();
            }
        }

        [HttpGet]
        public IEnumerable<PlateDetection> Get()
        {
            return null;
        }

        // GET: api/PlateDetection/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/PlateDetection
        [HttpPost]
        public async void Post([Bind]PlateDetection model)
        {
            if (ModelState.IsValid)
            {
                var files = HttpContext.Request.Form.Files;

                long size = files.Sum(f => f.Length);

                // full path to file in temp location
                var filePath = Path.GetTempFileName();

                foreach (var formFile in files)
                {
                    if (formFile.Length > 0)
                    {
                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                        }
                    }
                }

                //_context.Add(employee);
                //await _context.SaveChangesAsync();
                //return RedirectToAction("Index");

            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }
            //return View(employee);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
