﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Master_Backend.Models
{
    public class PlateDetection
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime Time { get; set; }
        [Required]
        public Dictionary<string, float> Results { get; set; }

        public string ImageName { get; set; }
        public string CroppedImageName { get; set; }

    }
}
