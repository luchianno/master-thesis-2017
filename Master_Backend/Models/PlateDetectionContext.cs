﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Master_Backend.Models
{
    public class PlateDetectionContext : DbContext
    {
        public PlateDetectionContext(DbContextOptions<PlateDetectionContext> options)
            : base(options)
        {
        }

        public DbSet<PlateDetection> Items { get; set; }
    }
}
